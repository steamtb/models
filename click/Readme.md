# Fattree Emulation

This is a fattree emulation implemented in
[fastclick](https://github.com/mergetb/fastclick), with extensions from
[moa](https://gitlab.com/mergetb/tech/moa). The emulation has the following
properties.

- 8 TOR switches
- 4 leaf switches
- 4 spine switches
- up to 96 emulated workers (mimes)

![](diagram.png)

The network is built from
[MultpathRouter](https://gitlab.com/mergetb/tech/moa/blob/rydev/click_elements/mprouter.hh) elements that implement a simple round-robin multipath routing scheme. Workers are implemented using [Mime](https://gitlab.com/mergetb/tech/moa/blob/rydev/click_elements/mime.hh) elements that emulate a workload parameterizable by a number of flows and the interleaving of computation and communication for each flow.
