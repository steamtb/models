/* fat8.click
 
 A fattree model

 - 8 tor
 - 4 leaf
 - 4 spine 
 - 96 node

 Split into green and blue planes. Naming is as follows

 <name>[B|G][i]

 <name>:
  - spine: a spine switch
  - leaf: a leaf switch
  - tor: a to switch
  - m: a mime

  [B|G]:
    - b: blue
    - g: green

  [i]:
    - an integer index
*/

// spines ---------------------------------------------------------------------

// green ~~~~

spineG0 :: MultipathRouter(
  // down
  10.0.0.0/24 0,
  10.0.1.0/24 0,
  10.0.2.0/24 0,
  10.0.3.0/24 0,

  10.0.4.0/24 1,
  10.0.5.0/24 1,
  10.0.6.0/24 1,
  10.0.7.0/24 1,
);

spineG1 :: MultipathRouter(
  // down
  10.0.0.0/24 0,
  10.0.1.0/24 0,
  10.0.2.0/24 0,
  10.0.3.0/24 0,

  10.0.4.0/24 1,
  10.0.5.0/24 1,
  10.0.6.0/24 1,
  10.0.7.0/24 1,
);

// blue ~~~~~

spineB0 :: MultipathRouter(
  // down
  10.0.0.0/24 0,
  10.0.1.0/24 0,
  10.0.2.0/24 0,
  10.0.3.0/24 0,

  10.0.4.0/24 1,
  10.0.5.0/24 1,
  10.0.6.0/24 1,
  10.0.7.0/24 1,
);

spineB1 :: MultipathRouter(
  // down
  10.0.0.0/24 0,
  10.0.1.0/24 0,
  10.0.2.0/24 0,
  10.0.3.0/24 0,

  10.0.4.0/24 1,
  10.0.5.0/24 1,
  10.0.6.0/24 1,
  10.0.7.0/24 1,
);

// leaves ---------------------------------------------------------------------

// green ~~~~

leafG0 :: MultipathRouter(
  // down
  10.0.0.0/24 0,
  10.0.1.0/24 1,
  10.0.2.0/24 2,
  10.0.3.0/24 3,

  //up
  10.0.4.0/24 4,
  10.0.4.0/24 5,

  10.0.5.0/24 4,
  10.0.5.0/24 5,

  10.0.6.0/24 4,
  10.0.6.0/24 5,

  10.0.7.0/24 4,
  10.0.7.0/24 5,
);

leafG1 :: MultipathRouter(
  // down
  10.0.4.0/24 0,
  10.0.5.0/24 1,
  10.0.6.0/24 2,
  10.0.7.0/24 3,

  //up
  10.0.0.0/24 4,
  10.0.0.0/24 5,

  10.0.1.0/24 4,
  10.0.1.0/24 5,

  10.0.2.0/24 4,
  10.0.2.0/24 5,

  10.0.3.0/24 4,
  10.0.3.0/24 5,
);


// blue ~~~~~

leafB0 :: MultipathRouter(
  // down
  10.0.0.0/24 0,
  10.0.1.0/24 1,
  10.0.2.0/24 2,
  10.0.3.0/24 3,

  //up
  10.0.4.0/24 4,
  10.0.4.0/24 5,

  10.0.5.0/24 4,
  10.0.5.0/24 5,

  10.0.6.0/24 4,
  10.0.6.0/24 5,

  10.0.7.0/24 4,
  10.0.7.0/24 5,
);

leafB1 :: MultipathRouter(
  // down
  10.0.4.0/24 0,
  10.0.5.0/24 1,
  10.0.6.0/24 2,
  10.0.7.0/24 3,

  //up
  10.0.0.0/24 4,
  10.0.0.0/24 5,

  10.0.1.0/24 4,
  10.0.1.0/24 5,

  10.0.2.0/24 4,
  10.0.2.0/24 5,

  10.0.3.0/24 4,
  10.0.3.0/24 5,
);

// TORs -----------------------------------------------------------------------

tor0 :: MultipathRouter(
  // down
  10.0.0.1/32 0,
  10.0.0.2/32 1,
  10.0.0.3/32 2,
  10.0.0.4/32 3,
  10.0.0.5/32 4,
  10.0.0.6/32 5,
  10.0.0.7/32 6,
  10.0.0.8/32 7,
  10.0.0.9/32 8,
  10.0.0.10/32 9,
  10.0.0.11/32 10,
  10.0.0.12/32 11,

  // up
  10.0.1.0/24 12,
  10.0.1.0/24 13,

  10.0.2.0/24 12,
  10.0.2.0/24 13,

  10.0.3.0/24 12,
  10.0.3.0/24 13,

  10.0.4.0/24 12,
  10.0.4.0/24 13,

  10.0.5.0/24 12,
  10.0.5.0/24 13,

  10.0.6.0/24 12,
  10.0.6.0/24 13,

  10.0.7.0/24 12,
  10.0.7.0/24 13,
);

tor1 :: MultipathRouter(
  // down
  10.0.1.1/32 0,
  10.0.1.2/32 1,
  10.0.1.3/32 2,
  10.0.1.4/32 3,
  10.0.1.5/32 4,
  10.0.1.6/32 5,
  10.0.1.7/32 6,
  10.0.1.8/32 7,
  10.0.1.9/32 8,
  10.0.1.10/32 9,
  10.0.1.11/32 10,
  10.0.1.12/32 11,
  
  // up
  10.0.0.0/24 12,
  10.0.0.0/24 13,

  10.0.2.0/24 12,
  10.0.2.0/24 13,

  10.0.3.0/24 12,
  10.0.3.0/24 13,

  10.0.4.0/24 12,
  10.0.4.0/24 13,

  10.0.5.0/24 12,
  10.0.5.0/24 13,

  10.0.6.0/24 12,
  10.0.6.0/24 13,

  10.0.7.0/24 12,
  10.0.7.0/24 13,
);

tor2 :: MultipathRouter(
  // down
  10.0.2.1/32 0,
  10.0.2.2/32 1,
  10.0.2.3/32 2,
  10.0.2.4/32 3,
  10.0.2.5/32 4,
  10.0.2.6/32 5,
  10.0.2.7/32 6,
  10.0.2.8/32 7,
  10.0.2.9/32 8,
  10.0.2.10/32 9,
  10.0.2.11/32 10,
  10.0.2.12/32 11,

  // up
  10.0.0.0/24 12,
  10.0.0.0/24 13,

  10.0.1.0/24 12,
  10.0.1.0/24 13,

  10.0.3.0/24 12,
  10.0.3.0/24 13,

  10.0.4.0/24 12,
  10.0.4.0/24 13,

  10.0.5.0/24 12,
  10.0.5.0/24 13,

  10.0.6.0/24 12,
  10.0.6.0/24 13,

  10.0.7.0/24 12,
  10.0.7.0/24 13,
);

tor3 :: MultipathRouter(
  // down
  10.0.3.1/32 0,
  10.0.3.2/32 1,
  10.0.3.3/32 2,
  10.0.3.4/32 3,
  10.0.3.5/32 4,
  10.0.3.6/32 5,
  10.0.3.7/32 6,
  10.0.3.8/32 7,
  10.0.3.9/32 8,
  10.0.3.10/32 9,
  10.0.3.11/32 10,
  10.0.3.12/32 11,

  // up
  10.0.0.0/24 12,
  10.0.0.0/24 13,

  10.0.1.0/24 12,
  10.0.1.0/24 13,

  10.0.2.0/24 12,
  10.0.2.0/24 13,

  10.0.4.0/24 12,
  10.0.4.0/24 13,

  10.0.5.0/24 12,
  10.0.5.0/24 13,

  10.0.6.0/24 12,
  10.0.6.0/24 13,

  10.0.7.0/24 12,
  10.0.7.0/24 13,
);

tor4 :: MultipathRouter(
  // down
  10.0.4.1/32 0,
  10.0.4.2/32 1,
  10.0.4.3/32 2,
  10.0.4.4/32 3,
  10.0.4.5/32 4,
  10.0.4.6/32 5,
  10.0.4.7/32 6,
  10.0.4.8/32 7,
  10.0.4.9/32 8,
  10.0.4.10/32 9,
  10.0.4.11/32 10,
  10.0.4.12/32 11,

  // up
  10.0.0.0/24 12,
  10.0.0.0/24 13,

  10.0.1.0/24 12,
  10.0.1.0/24 13,

  10.0.2.0/24 12,
  10.0.2.0/24 13,

  10.0.3.0/24 12,
  10.0.3.0/24 13,

  10.0.5.0/24 12,
  10.0.5.0/24 13,

  10.0.6.0/24 12,
  10.0.6.0/24 13,

  10.0.7.0/24 12,
  10.0.7.0/24 13,
);

tor5 :: MultipathRouter(
  // down
  10.0.5.1/32 0,
  10.0.5.2/32 1,
  10.0.5.3/32 2,
  10.0.5.4/32 3,
  10.0.5.5/32 4,
  10.0.5.6/32 5,
  10.0.5.7/32 6,
  10.0.5.8/32 7,
  10.0.5.9/32 8,
  10.0.5.10/32 9,
  10.0.5.11/32 10,
  10.0.5.12/32 11,
  
  // up
  10.0.0.0/24 12,
  10.0.0.0/24 13,

  10.0.1.0/24 12,
  10.0.1.0/24 13,

  10.0.2.0/24 12,
  10.0.2.0/24 13,

  10.0.3.0/24 12,
  10.0.3.0/24 13,

  10.0.4.0/24 12,
  10.0.4.0/24 13,

  10.0.6.0/24 12,
  10.0.6.0/24 13,

  10.0.7.0/24 12,
  10.0.7.0/24 13,
);

tor6 :: MultipathRouter(
  // down
  10.0.6.1/32 0,
  10.0.6.2/32 1,
  10.0.6.3/32 2,
  10.0.6.4/32 3,
  10.0.6.5/32 4,
  10.0.6.6/32 5,
  10.0.6.7/32 6,
  10.0.6.8/32 7,
  10.0.6.9/32 8,
  10.0.6.10/32 9,
  10.0.6.11/32 10,
  10.0.6.12/32 11,

  // up
  10.0.0.0/24 12,
  10.0.0.0/24 13,

  10.0.1.0/24 12,
  10.0.1.0/24 13,

  10.0.2.0/24 12,
  10.0.2.0/24 13,

  10.0.3.0/24 12,
  10.0.3.0/24 13,

  10.0.4.0/24 12,
  10.0.4.0/24 13,

  10.0.5.0/24 12,
  10.0.5.0/24 13,

  10.0.7.0/24 12,
  10.0.7.0/24 13,
);

tor7 :: MultipathRouter(
  // down
  10.0.7.1/32 0,
  10.0.7.2/32 1,
  10.0.7.3/32 2,
  10.0.7.4/32 3,
  10.0.7.5/32 4,
  10.0.7.6/32 5,
  10.0.7.7/32 6,
  10.0.7.8/32 7,
  10.0.7.9/32 8,
  10.0.7.10/32 9,
  10.0.7.11/32 10,
  10.0.7.12/32 11,

  // up
  10.0.0.0/24 12,
  10.0.0.0/24 13,

  10.0.1.0/24 12,
  10.0.1.0/24 13,

  10.0.2.0/24 12,
  10.0.2.0/24 13,

  10.0.3.0/24 12,
  10.0.3.0/24 13,

  10.0.4.0/24 12,
  10.0.4.0/24 13,

  10.0.5.0/24 12,
  10.0.5.0/24 13,

  10.0.6.0/24 12,
  10.0.6.0/24 13,
);

// mimes ----------------------------------------------------------------------

define(
  $T1     1000ms,
  $T2     1ms,
  $DATA   100000000,
  $MINPKT 60000, // FIXME implicit packet batching
  $MAXPKT 60000
);

// senders ~~~~~~~~~~~~~~~~~~~~~~


// rack 0 ++++++

m01 :: Mime(
  IP 10.0.0.1,
  MAC 00:00:00:00:00:01,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.1,
  MAC0     00:00:00:00:04:01,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.1,
  MAC1     00:00:00:00:04:01,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,

  STOP true
);

m02 :: Mime(
  IP 10.0.0.2,
  MAC 00:00:00:00:00:02,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.2,
  MAC0     00:00:00:00:04:02,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.2,
  MAC1     00:00:00:00:04:02,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m03 :: Mime(
  IP 10.0.0.3,
  MAC 00:00:00:00:00:03,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.3,
  MAC0     00:00:00:00:04:03,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.3,
  MAC1     00:00:00:00:04:03,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m04 :: Mime(
  IP 10.0.0.4,
  MAC 00:00:00:00:00:04,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.4,
  MAC0     00:00:00:00:04:04,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.4,
  MAC1     00:00:00:00:04:04,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m05 :: Mime(
  IP 10.0.0.5,
  MAC 00:00:00:00:00:05,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.5,
  MAC0     00:00:00:00:04:05,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.5,
  MAC1     00:00:00:00:04:05,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m06 :: Mime(
  IP 10.0.0.6,
  MAC 00:00:00:00:00:05,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.6,
  MAC0     00:00:00:00:04:06,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.6,
  MAC1     00:00:00:00:04:06,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m07 :: Mime(
  IP 10.0.0.7,
  MAC 00:00:00:00:00:07,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.7,
  MAC0     00:00:00:00:04:07,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.7,
  MAC1     00:00:00:00:04:07,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m08 :: Mime(
  IP 10.0.0.8,
  MAC 00:00:00:00:00:08,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.8,
  MAC0     00:00:00:00:04:08,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.8,
  MAC1     00:00:00:00:04:08,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m09 :: Mime(
  IP 10.0.0.9,
  MAC 00:00:00:00:00:09,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.9,
  MAC0     00:00:00:00:04:09,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.9,
  MAC1     00:00:00:00:04:09,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m010 :: Mime(
  IP 10.0.0.10,
  MAC 00:00:00:00:00:0a,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.10,
  MAC0     00:00:00:00:04:0a,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.10,
  MAC1     00:00:00:00:04:0a,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m011 :: Mime(
  IP 10.0.0.11,
  MAC 00:00:00:00:00:0b,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.11,
  MAC0     00:00:00:00:04:0b,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.11,
  MAC1     00:00:00:00:04:0b,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m012 :: Mime(
  IP 10.0.0.12,
  MAC 00:00:00:00:00:0c,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.4.12,
  MAC0     00:00:00:00:04:0c,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.4.12,
  MAC1     00:00:00:00:04:0c,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

// rack 1 ++++++

m11 :: Mime(
  IP 10.0.1.1,
  MAC 00:00:00:00:01:01,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.1,
  MAC0     00:00:00:00:05:01,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.1,
  MAC1     00:00:00:00:05:01,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m12 :: Mime(
  IP 10.0.1.2,
  MAC 00:00:00:00:01:02,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.2,
  MAC0     00:00:00:00:05:02,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.2,
  MAC1     00:00:00:00:05:02,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m13 :: Mime(
  IP 10.0.1.3,
  MAC 00:00:00:00:01:03,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.3,
  MAC0     00:00:00:00:05:03,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.3,
  MAC1     00:00:00:00:05:03,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m14 :: Mime(
  IP 10.0.1.4,
  MAC 00:00:00:00:01:04,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.4,
  MAC0     00:00:00:00:05:04,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.4,
  MAC1     00:00:00:00:05:04,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m15 :: Mime(
  IP 10.0.1.5,
  MAC 00:00:00:00:01:05,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.5,
  MAC0     00:00:00:00:05:05,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.5,
  MAC1     00:00:00:00:05:05,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m16 :: Mime(
  IP 10.0.1.6,
  MAC 00:00:00:00:01:06,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.6,
  MAC0     00:00:00:00:05:06,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.6,
  MAC1     00:00:00:00:05:06,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m17 :: Mime(
  IP 10.0.1.7,
  MAC 00:00:00:00:01:07,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.7,
  MAC0     00:00:00:00:05:07,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.7,
  MAC1     00:00:00:00:05:07,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m18 :: Mime(
  IP 10.0.1.8,
  MAC 00:00:00:00:01:08,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.8,
  MAC0     00:00:00:00:05:08,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.8,
  MAC1     00:00:00:00:05:08,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m19 :: Mime(
  IP 10.0.1.9,
  MAC 00:00:00:00:01:09,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.9,
  MAC0     00:00:00:00:05:09,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.9,
  MAC1     00:00:00:00:05:09,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m110 :: Mime(
  IP 10.0.1.10,
  MAC 00:00:00:00:01:0a,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.10,
  MAC0     00:00:00:00:05:0a,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.10,
  MAC1     00:00:00:00:05:0a,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m111 :: Mime(
  IP 10.0.1.11,
  MAC 00:00:00:00:01:0b,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.11,
  MAC0     00:00:00:00:05:0b,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.11,
  MAC1     00:00:00:00:05:0b,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m112 :: Mime(
  IP 10.0.1.12,
  MAC 00:00:00:00:01:0c,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.5.12,
  MAC0     00:00:00:00:05:0c,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.5.12,
  MAC1     00:00:00:00:05:0c,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

// rack 2 ++++++

m21 :: Mime(
  IP 10.0.2.1,
  MAC 00:00:00:00:02:01,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.1,
  MAC0     00:00:00:00:06:01,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.1,
  MAC1     00:00:00:00:06:01,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m22 :: Mime(
  IP 10.0.2.2,
  MAC 00:00:00:00:02:02,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.2,
  MAC0     00:00:00:00:06:02,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.2,
  MAC1     00:00:00:00:06:02,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m23 :: Mime(
  IP 10.0.2.3,
  MAC 00:00:00:00:02:03,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.3,
  MAC0     00:00:00:00:06:03,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.3,
  MAC1     00:00:00:00:06:03,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m24 :: Mime(
  IP 10.0.2.4,
  MAC 00:00:00:00:02:04,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.4,
  MAC0     00:00:00:00:06:04,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.4,
  MAC1     00:00:00:00:06:04,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m25 :: Mime(
  IP 10.0.2.5,
  MAC 00:00:00:00:02:05,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.5,
  MAC0     00:00:00:00:06:05,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.5,
  MAC1     00:00:00:00:06:05,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m26 :: Mime(
  IP 10.0.2.6,
  MAC 00:00:00:00:02:06,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.6,
  MAC0     00:00:00:00:06:06,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.6,
  MAC1     00:00:00:00:06:06,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m27 :: Mime(
  IP 10.0.2.7,
  MAC 00:00:00:00:02:07,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.7,
  MAC0     00:00:00:00:06:07,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.7,
  MAC1     00:00:00:00:06:07,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m28 :: Mime(
  IP 10.0.2.8,
  MAC 00:00:00:00:02:08,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.8,
  MAC0     00:00:00:00:06:08,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.8,
  MAC1     00:00:00:00:06:08,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m29 :: Mime(
  IP 10.0.2.9,
  MAC 00:00:00:00:02:09,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.9,
  MAC0     00:00:00:00:06:09,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.9,
  MAC1     00:00:00:00:06:09,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m210 :: Mime(
  IP 10.0.2.10,
  MAC 00:00:00:00:02:0a,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.10,
  MAC0     00:00:00:00:06:0a,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.10,
  MAC1     00:00:00:00:06:0a,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m211 :: Mime(
  IP 10.0.2.11,
  MAC 00:00:00:00:02:0b,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.11,
  MAC0     00:00:00:00:06:0b,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.11,
  MAC1     00:00:00:00:06:0b,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m212 :: Mime(
  IP 10.0.2.12,
  MAC 00:00:00:00:02:0c,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.6.12,
  MAC0     00:00:00:00:06:0c,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.6.12,
  MAC1     00:00:00:00:06:0c,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

// rack 3 ++++++

m31 :: Mime(
  IP 10.0.3.1,
  MAC 00:00:00:00:03:01,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.1,
  MAC0     00:00:00:00:07:01,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.1,
  MAC1     00:00:00:00:07:01,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m32 :: Mime(
  IP 10.0.3.2,
  MAC 00:00:00:00:03:02,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.2,
  MAC0     00:00:00:00:07:02,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.2,
  MAC1     00:00:00:00:07:02,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m33 :: Mime(
  IP 10.0.3.3,
  MAC 00:00:00:00:03:03,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.3,
  MAC0     00:00:00:00:07:03,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.3,
  MAC1     00:00:00:00:07:03,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m34 :: Mime(
  IP 10.0.3.4,
  MAC 00:00:00:00:03:04,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.4,
  MAC0     00:00:00:00:07:04,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.4,
  MAC1     00:00:00:00:07:04,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m35 :: Mime(
  IP 10.0.3.5,
  MAC 00:00:00:00:03:05,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.5,
  MAC0     00:00:00:00:07:05,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.5,
  MAC1     00:00:00:00:07:05,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m36 :: Mime(
  IP 10.0.3.6,
  MAC 00:00:00:00:03:06,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.6,
  MAC0     00:00:00:00:07:06,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.6,
  MAC1     00:00:00:00:07:06,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m37 :: Mime(
  IP 10.0.3.7,
  MAC 00:00:00:00:03:07,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.7,
  MAC0     00:00:00:00:07:07,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.7,
  MAC1     00:00:00:00:07:07,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m38 :: Mime(
  IP 10.0.3.8,
  MAC 00:00:00:00:03:08,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.8,
  MAC0     00:00:00:00:07:08,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.8,
  MAC1     00:00:00:00:07:08,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m39 :: Mime(
  IP 10.0.3.9,
  MAC 00:00:00:00:03:09,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.9,
  MAC0     00:00:00:00:07:09,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.9,
  MAC1     00:00:00:00:07:09,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m310 :: Mime(
  IP 10.0.3.10,
  MAC 00:00:00:00:03:0a,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.10,
  MAC0     00:00:00:00:07:0a,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.10,
  MAC1     00:00:00:00:07:0a,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m311 :: Mime(
  IP 10.0.3.11,
  MAC 00:00:00:00:03:0b,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.11,
  MAC0     00:00:00:00:07:0b,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.11,
  MAC1     00:00:00:00:07:0b,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

m312 :: Mime(
  IP 10.0.3.12,
  MAC 00:00:00:00:03:0c,

  COMPUTE0 $T1,
  DATA0    $DATA,
  DST0     10.0.7.12,
  MAC0     00:00:00:00:07:0c,
  MAXPKT0  $MAXPKT,
  MINPKT0  $MINPKT,

  COMPUTE1 $T1,
  DATA1    $DATA,
  DST1     10.0.7.12,
  MAC1     00:00:00:00:07:0c,
  MAXPKT1  $MAXPKT,
  MINPKT1  $MINPKT,
);

// receivers ~~~

// rack 4 ++++++

m41 :: Mime(
  IP 10.0.4.1,
  MAC 00:00:00:00:04:01,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m42 :: Mime(
  IP 10.0.4.2,
  MAC 00:00:00:00:04:02,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m43 :: Mime(
  IP 10.0.4.3,
  MAC 00:00:00:00:04:03,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m44 :: Mime(
  IP 10.0.4.4,
  MAC 00:00:00:00:04:04,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m45 :: Mime(
  IP 10.0.4.5,
  MAC 00:00:00:00:04:05,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m46 :: Mime(
  IP 10.0.4.6,
  MAC 00:00:00:00:04:06,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m47 :: Mime(
  IP 10.0.4.7,
  MAC 00:00:00:00:04:07,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m48 :: Mime(
  IP 10.0.4.8,
  MAC 00:00:00:00:04:08,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m49 :: Mime(
  IP 10.0.4.9,
  MAC 00:00:00:00:04:09,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m410 :: Mime(
  IP 10.0.4.10,
  MAC 00:00:00:00:04:0a,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m411 :: Mime(
  IP 10.0.4.11,
  MAC 00:00:00:00:04:0b,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m412 :: Mime(
  IP 10.0.4.12,
  MAC 00:00:00:00:04:0c,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

// rack 5 ++++++

m51 :: Mime(
  IP 10.0.5.1,
  MAC 00:00:00:00:05:01,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m52 :: Mime(
  IP 10.0.5.2,
  MAC 00:00:00:00:05:02,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m53 :: Mime(
  IP 10.0.5.3,
  MAC 00:00:00:00:05:03,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m54 :: Mime(
  IP 10.0.5.4,
  MAC 00:00:00:00:05:04,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m55 :: Mime(
  IP 10.0.5.5,
  MAC 00:00:00:00:05:05,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m56 :: Mime(
  IP 10.0.5.6,
  MAC 00:00:00:00:05:06,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m57 :: Mime(
  IP 10.0.5.7,
  MAC 00:00:00:00:05:07,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m58 :: Mime(
  IP 10.0.5.8,
  MAC 00:00:00:00:05:08,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m59 :: Mime(
  IP 10.0.5.9,
  MAC 00:00:00:00:05:09,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m510 :: Mime(
  IP 10.0.5.10,
  MAC 00:00:00:00:05:0a,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m511 :: Mime(
  IP 10.0.5.11,
  MAC 00:00:00:00:05:0b,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m512 :: Mime(
  IP 10.0.5.12,
  MAC 00:00:00:00:05:0c,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

// rack 6 ++++++

m61 :: Mime(
  IP 10.0.6.1,
  MAC 00:00:00:00:06:01,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m62 :: Mime(
  IP 10.0.6.2,
  MAC 00:00:00:00:06:02,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m63 :: Mime(
  IP 10.0.6.3,
  MAC 00:00:00:00:06:03,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m64 :: Mime(
  IP 10.0.6.4,
  MAC 00:00:00:00:06:04,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m65 :: Mime(
  IP 10.0.6.5,
  MAC 00:00:00:00:06:05,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m66 :: Mime(
  IP 10.0.6.6,
  MAC 00:00:00:00:06:06,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m67 :: Mime(
  IP 10.0.6.7,
  MAC 00:00:00:00:06:07,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m68 :: Mime(
  IP 10.0.6.8,
  MAC 00:00:00:00:06:08,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m69 :: Mime(
  IP 10.0.6.9,
  MAC 00:00:00:00:06:09,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m610 :: Mime(
  IP 10.0.6.10,
  MAC 00:00:00:00:06:0a,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m611 :: Mime(
  IP 10.0.6.11,
  MAC 00:00:00:00:06:0b,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m612 :: Mime(
  IP 10.0.6.12,
  MAC 00:00:00:00:06:0c,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

// rack 7 ++++++

m71 :: Mime(
  IP 10.0.7.1,
  MAC 00:00:00:00:07:01,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m72 :: Mime(
  IP 10.0.7.2,
  MAC 00:00:00:00:07:02,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m73 :: Mime(
  IP 10.0.7.3,
  MAC 00:00:00:00:07:03,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m74 :: Mime(
  IP 10.0.7.4,
  MAC 00:00:00:00:07:04,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m75 :: Mime(
  IP 10.0.7.5,
  MAC 00:00:00:00:07:05,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m76 :: Mime(
  IP 10.0.7.6,
  MAC 00:00:00:00:07:06,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m77 :: Mime(
  IP 10.0.7.7,
  MAC 00:00:00:00:07:07,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m78 :: Mime(
  IP 10.0.7.8,
  MAC 00:00:00:00:07:08,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m79 :: Mime(
  IP 10.0.7.9,
  MAC 00:00:00:00:07:09,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m710 :: Mime(
  IP 10.0.7.10,
  MAC 00:00:00:00:07:0a,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m711 :: Mime(
  IP 10.0.7.11,
  MAC 00:00:00:00:07:0b,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

m712 :: Mime(
  IP 10.0.7.12,
  MAC 00:00:00:00:07:0c,

  COMPUTE0 $T2,
  COMPUTE1 $T2
);

// links ======================================================================

define(
  $HOP  10us,
  $CORE 10Gbps,
  $EDGE 1Gbps,
  $Q    1000000,
);

elementclass EdgeLink {
  input -> Queue($Q) -> LinkUnqueue($HOP, $EDGE) -> output;
}

elementclass CoreLink {
  input -> Queue($Q) -> LinkUnqueue($HOP, $CORE) -> output;
}

// mime ~~~> tor

// senders
m01[0]  -> EdgeLink ->  [0]tor0[0]  -> EdgeLink -> Queue($Q) -> [0]m01;
m02[0]  -> EdgeLink ->  [1]tor0[1]  -> EdgeLink -> Queue($Q) -> [0]m02;
m03[0]  -> EdgeLink ->  [2]tor0[2]  -> EdgeLink -> Queue($Q) -> [0]m03;
m04[0]  -> EdgeLink ->  [3]tor0[3]  -> EdgeLink -> Queue($Q) -> [0]m04;
m05[0]  -> EdgeLink ->  [4]tor0[4]  -> EdgeLink -> Queue($Q) -> [0]m05;
m06[0]  -> EdgeLink ->  [5]tor0[5]  -> EdgeLink -> Queue($Q) -> [0]m06;
m07[0]  -> EdgeLink ->  [6]tor0[6]  -> EdgeLink -> Queue($Q) -> [0]m07;
m08[0]  -> EdgeLink ->  [7]tor0[7]  -> EdgeLink -> Queue($Q) -> [0]m08;
m09[0]  -> EdgeLink ->  [8]tor0[8]  -> EdgeLink -> Queue($Q) -> [0]m09;
m010[0] -> EdgeLink ->  [9]tor0[9]  -> EdgeLink -> Queue($Q) -> [0]m010;
m011[0] -> EdgeLink -> [10]tor0[10] -> EdgeLink -> Queue($Q) -> [0]m011;
m012[0] -> EdgeLink -> [11]tor0[11] -> EdgeLink -> Queue($Q) -> [0]m012;

m11[0]  -> EdgeLink ->  [0]tor1[0]  -> EdgeLink -> Queue($Q) -> [0]m11;
m12[0]  -> EdgeLink ->  [1]tor1[1]  -> EdgeLink -> Queue($Q) -> [0]m12;
m13[0]  -> EdgeLink ->  [2]tor1[2]  -> EdgeLink -> Queue($Q) -> [0]m13;
m14[0]  -> EdgeLink ->  [3]tor1[3]  -> EdgeLink -> Queue($Q) -> [0]m14;
m15[0]  -> EdgeLink ->  [4]tor1[4]  -> EdgeLink -> Queue($Q) -> [0]m15;
m16[0]  -> EdgeLink ->  [5]tor1[5]  -> EdgeLink -> Queue($Q) -> [0]m16;
m17[0]  -> EdgeLink ->  [6]tor1[6]  -> EdgeLink -> Queue($Q) -> [0]m17;
m18[0]  -> EdgeLink ->  [7]tor1[7]  -> EdgeLink -> Queue($Q) -> [0]m18;
m19[0]  -> EdgeLink ->  [8]tor1[8]  -> EdgeLink -> Queue($Q) -> [0]m19;
m110[0] -> EdgeLink ->  [9]tor1[9]  -> EdgeLink -> Queue($Q) -> [0]m110;
m111[0] -> EdgeLink -> [10]tor1[10] -> EdgeLink -> Queue($Q) -> [0]m111;
m112[0] -> EdgeLink -> [11]tor1[11] -> EdgeLink -> Queue($Q) -> [0]m112;

m21[0]  -> EdgeLink ->  [0]tor2[0]  -> EdgeLink -> Queue($Q) -> [0]m21;
m22[0]  -> EdgeLink ->  [1]tor2[1]  -> EdgeLink -> Queue($Q) -> [0]m22;
m23[0]  -> EdgeLink ->  [2]tor2[2]  -> EdgeLink -> Queue($Q) -> [0]m23;
m24[0]  -> EdgeLink ->  [3]tor2[3]  -> EdgeLink -> Queue($Q) -> [0]m24;
m25[0]  -> EdgeLink ->  [4]tor2[4]  -> EdgeLink -> Queue($Q) -> [0]m25;
m26[0]  -> EdgeLink ->  [5]tor2[5]  -> EdgeLink -> Queue($Q) -> [0]m26;
m27[0]  -> EdgeLink ->  [6]tor2[6]  -> EdgeLink -> Queue($Q) -> [0]m27;
m28[0]  -> EdgeLink ->  [7]tor2[7]  -> EdgeLink -> Queue($Q) -> [0]m28;
m29[0]  -> EdgeLink ->  [8]tor2[8]  -> EdgeLink -> Queue($Q) -> [0]m29;
m210[0] -> EdgeLink ->  [9]tor2[9]  -> EdgeLink -> Queue($Q) -> [0]m210;
m211[0] -> EdgeLink -> [10]tor2[10] -> EdgeLink -> Queue($Q) -> [0]m211;
m212[0] -> EdgeLink -> [11]tor2[11] -> EdgeLink -> Queue($Q) -> [0]m212;

m31[0]  -> EdgeLink ->  [0]tor3[0]  -> EdgeLink -> Queue($Q) -> [0]m31;
m32[0]  -> EdgeLink ->  [1]tor3[1]  -> EdgeLink -> Queue($Q) -> [0]m32;
m33[0]  -> EdgeLink ->  [2]tor3[2]  -> EdgeLink -> Queue($Q) -> [0]m33;
m34[0]  -> EdgeLink ->  [3]tor3[3]  -> EdgeLink -> Queue($Q) -> [0]m34;
m35[0]  -> EdgeLink ->  [4]tor3[4]  -> EdgeLink -> Queue($Q) -> [0]m35;
m36[0]  -> EdgeLink ->  [5]tor3[5]  -> EdgeLink -> Queue($Q) -> [0]m36;
m37[0]  -> EdgeLink ->  [6]tor3[6]  -> EdgeLink -> Queue($Q) -> [0]m37;
m38[0]  -> EdgeLink ->  [7]tor3[7]  -> EdgeLink -> Queue($Q) -> [0]m38;
m39[0]  -> EdgeLink ->  [8]tor3[8]  -> EdgeLink -> Queue($Q) -> [0]m39;
m310[0] -> EdgeLink ->  [9]tor3[9]  -> EdgeLink -> Queue($Q) -> [0]m310;
m311[0] -> EdgeLink -> [10]tor3[10] -> EdgeLink -> Queue($Q) -> [0]m311;
m312[0] -> EdgeLink -> [11]tor3[11] -> EdgeLink -> Queue($Q) -> [0]m312;

//receivers
m41[0]  -> EdgeLink ->  [0]tor4[0]  -> EdgeLink -> Queue($Q) -> [0]m41;
m42[0]  -> EdgeLink ->  [1]tor4[1]  -> EdgeLink -> Queue($Q) -> [0]m42;
m43[0]  -> EdgeLink ->  [2]tor4[2]  -> EdgeLink -> Queue($Q) -> [0]m43;
m44[0]  -> EdgeLink ->  [3]tor4[3]  -> EdgeLink -> Queue($Q) -> [0]m44;
m45[0]  -> EdgeLink ->  [4]tor4[4]  -> EdgeLink -> Queue($Q) -> [0]m45;
m46[0]  -> EdgeLink ->  [5]tor4[5]  -> EdgeLink -> Queue($Q) -> [0]m46;
m47[0]  -> EdgeLink ->  [6]tor4[6]  -> EdgeLink -> Queue($Q) -> [0]m47;
m48[0]  -> EdgeLink ->  [7]tor4[7]  -> EdgeLink -> Queue($Q) -> [0]m48;
m49[0]  -> EdgeLink ->  [8]tor4[8]  -> EdgeLink -> Queue($Q) -> [0]m49;
m410[0] -> EdgeLink ->  [9]tor4[9]  -> EdgeLink -> Queue($Q) -> [0]m410;
m411[0] -> EdgeLink -> [10]tor4[10] -> EdgeLink -> Queue($Q) -> [0]m411;
m412[0] -> EdgeLink -> [11]tor4[11] -> EdgeLink -> Queue($Q) -> [0]m412;

m51[0]  -> EdgeLink ->  [0]tor5[0]  -> EdgeLink -> Queue($Q) -> [0]m51;
m52[0]  -> EdgeLink ->  [1]tor5[1]  -> EdgeLink -> Queue($Q) -> [0]m52;
m53[0]  -> EdgeLink ->  [2]tor5[2]  -> EdgeLink -> Queue($Q) -> [0]m53;
m54[0]  -> EdgeLink ->  [3]tor5[3]  -> EdgeLink -> Queue($Q) -> [0]m54;
m55[0]  -> EdgeLink ->  [4]tor5[4]  -> EdgeLink -> Queue($Q) -> [0]m55;
m56[0]  -> EdgeLink ->  [5]tor5[5]  -> EdgeLink -> Queue($Q) -> [0]m56;
m57[0]  -> EdgeLink ->  [6]tor5[6]  -> EdgeLink -> Queue($Q) -> [0]m57;
m58[0]  -> EdgeLink ->  [7]tor5[7]  -> EdgeLink -> Queue($Q) -> [0]m58;
m59[0]  -> EdgeLink ->  [8]tor5[8]  -> EdgeLink -> Queue($Q) -> [0]m59;
m510[0] -> EdgeLink ->  [9]tor5[9]  -> EdgeLink -> Queue($Q) -> [0]m510;
m511[0] -> EdgeLink -> [10]tor5[10] -> EdgeLink -> Queue($Q) -> [0]m511;
m512[0] -> EdgeLink -> [11]tor5[11] -> EdgeLink -> Queue($Q) -> [0]m512;

m61[0]  -> EdgeLink ->  [0]tor6[0]  -> EdgeLink -> Queue($Q) -> [0]m61;
m62[0]  -> EdgeLink ->  [1]tor6[1]  -> EdgeLink -> Queue($Q) -> [0]m62;
m63[0]  -> EdgeLink ->  [2]tor6[2]  -> EdgeLink -> Queue($Q) -> [0]m63;
m64[0]  -> EdgeLink ->  [3]tor6[3]  -> EdgeLink -> Queue($Q) -> [0]m64;
m65[0]  -> EdgeLink ->  [4]tor6[4]  -> EdgeLink -> Queue($Q) -> [0]m65;
m66[0]  -> EdgeLink ->  [5]tor6[5]  -> EdgeLink -> Queue($Q) -> [0]m66;
m67[0]  -> EdgeLink ->  [6]tor6[6]  -> EdgeLink -> Queue($Q) -> [0]m67;
m68[0]  -> EdgeLink ->  [7]tor6[7]  -> EdgeLink -> Queue($Q) -> [0]m68;
m69[0]  -> EdgeLink ->  [8]tor6[8]  -> EdgeLink -> Queue($Q) -> [0]m69;
m610[0] -> EdgeLink ->  [9]tor6[9]  -> EdgeLink -> Queue($Q) -> [0]m610;
m611[0] -> EdgeLink -> [10]tor6[10] -> EdgeLink -> Queue($Q) -> [0]m611;
m612[0] -> EdgeLink -> [11]tor6[11] -> EdgeLink -> Queue($Q) -> [0]m612;

m71[0]  -> EdgeLink ->  [0]tor7[0]  -> EdgeLink -> Queue($Q) -> [0]m71;
m72[0]  -> EdgeLink ->  [1]tor7[1]  -> EdgeLink -> Queue($Q) -> [0]m72;
m73[0]  -> EdgeLink ->  [2]tor7[2]  -> EdgeLink -> Queue($Q) -> [0]m73;
m74[0]  -> EdgeLink ->  [3]tor7[3]  -> EdgeLink -> Queue($Q) -> [0]m74;
m75[0]  -> EdgeLink ->  [4]tor7[4]  -> EdgeLink -> Queue($Q) -> [0]m75;
m76[0]  -> EdgeLink ->  [5]tor7[5]  -> EdgeLink -> Queue($Q) -> [0]m76;
m77[0]  -> EdgeLink ->  [6]tor7[6]  -> EdgeLink -> Queue($Q) -> [0]m77;
m78[0]  -> EdgeLink ->  [7]tor7[7]  -> EdgeLink -> Queue($Q) -> [0]m78;
m79[0]  -> EdgeLink ->  [8]tor7[8]  -> EdgeLink -> Queue($Q) -> [0]m79;
m710[0] -> EdgeLink ->  [9]tor7[9]  -> EdgeLink -> Queue($Q) -> [0]m710;
m711[0] -> EdgeLink -> [10]tor7[10] -> EdgeLink -> Queue($Q) -> [0]m711;
m712[0] -> EdgeLink -> [11]tor7[11] -> EdgeLink -> Queue($Q) -> [0]m712;

// tor ~~~> leaf

tor0[12] -> CoreLink -> [0]leafG0[0] -> CoreLink -> [12]tor0;
tor0[13] -> CoreLink -> [0]leafB0[0] -> CoreLink -> [13]tor0;

tor1[12] -> CoreLink -> [1]leafG0[1] -> CoreLink -> [12]tor1;
tor1[13] -> CoreLink -> [1]leafB0[1] -> CoreLink -> [13]tor1;

tor2[12] -> CoreLink -> [2]leafG0[2] -> CoreLink -> [12]tor2;
tor2[13] -> CoreLink -> [2]leafB0[2] -> CoreLink -> [13]tor2;

tor3[12] -> CoreLink -> [3]leafG0[3] -> CoreLink -> [12]tor3;
tor3[13] -> CoreLink -> [3]leafB0[3] -> CoreLink -> [13]tor3;

//

tor4[12] -> CoreLink -> [0]leafG1[0] -> CoreLink -> [12]tor4;
tor4[13] -> CoreLink -> [0]leafB1[0] -> CoreLink -> [13]tor4;

tor5[12] -> CoreLink -> [1]leafG1[1] -> CoreLink -> [12]tor5;
tor5[13] -> CoreLink -> [1]leafB1[1] -> CoreLink -> [13]tor5;

tor6[12] -> CoreLink -> [2]leafG1[2] -> CoreLink -> [12]tor6;
tor6[13] -> CoreLink -> [2]leafB1[2] -> CoreLink -> [13]tor6;

tor7[12] -> CoreLink -> [3]leafG1[3] -> CoreLink -> [12]tor7;
tor7[13] -> CoreLink -> [3]leafB1[3] -> CoreLink -> [13]tor7;

// leaf ~~~> spine

leafG0[4] -> CoreLink -> [0]spineG0[0] -> CoreLink -> [4]leafG0;
leafG0[5] -> CoreLink -> [0]spineG1[0] -> CoreLink -> [5]leafG0;
leafG1[4] -> CoreLink -> [1]spineG0[1] -> CoreLink -> [4]leafG1;
leafG1[5] -> CoreLink -> [1]spineG1[1] -> CoreLink -> [5]leafG1;

leafB0[4] -> CoreLink -> [0]spineB0[0] -> CoreLink -> [4]leafB0;
leafB0[5] -> CoreLink -> [0]spineB1[0] -> CoreLink -> [5]leafB0;
leafB1[4] -> CoreLink -> [1]spineB0[1] -> CoreLink -> [4]leafB1;
leafB1[5] -> CoreLink -> [1]spineB1[1] -> CoreLink -> [5]leafB1;

/*
DriverManager(
  label loop,
  wait 0.1s,
  set done true,
  set done $(and $done m01.finished),
  set done $(and $done m02.finished),
  goto loop $(done),
  stop
);
*/


