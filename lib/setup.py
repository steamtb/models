from setuptools import setup

setup(  name='steam',
        version='0.1',
        description='steam model library',
        url='https://github.com/steamtb/models',
        author='Ryan Goodfellow',
        author_email='rgoodfel@isi.edu',
        license='Apache2.0',
        packages=['steam'],
        include_package_data=True,
        zip_safe=False
        )
