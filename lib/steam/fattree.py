import os,sys
import xir
import math
from .net import switch
from .pci import PciClass

# define a switch as a set of uplinks and a set of downlinks
def switch_def(up, down):
    return { 
        'up': {'count': up[0], 'bw': xir.gbps(up[1])},
        'down': {'count': down[0], 'bw': xir.gbps(down[1])} 
    }

def fbfattree_stats(tor_down, tor_up, fabric_radix, spine_radix, osr):
    
    n_plane = tor_up
    max_pod_size = tor_down * int(fabric_radix/2)
    max_pods = spine_radix
    
    print("max planes %d"%n_plane)
    print("max pod size %d"%max_pod_size)
    print("max pods %d"%max_pods)
    print("max nodes %d"%(max_pod_size*max_pods))

def torswitch(name, down, up, base):
    """This model is roughly based on the 10-down 100-up Mellanox MSN2410, it's
       a TOR switch with 48 downlinks and 10 uplinks where the difference in
       between the downlinks and uplinks is an order of magnitude"""
    ports = ([xir.Endpoint({'type': 'ethernet', 'speed': xir.gbps(base)}) for _ in range(down)] +
             [xir.Endpoint({'type': 'ethernet', 'speed': xir.gbps(base*10)}) for _ in range(up)])
    return switch(name, ports)

def coreswitch(name, radix, base):
    """This is a simple switch with @radix ports all with @base bandwidth per
       port"""
    ports = [xir.Endpoint({'type': 'ethernet', 'speed': xir.gbps(base)}) for _ in range(radix)]
    return switch(name, ports)

def fbfattree_build(
        n_pods, tor_down, tor_up, fabric_radix, spine_radix, n_plane, osr,
        node_bw, core_bw,
        node
        ):

    # compute the counts for everything
    if n_plane > tor_up:
        raise "# planes > # tor uplinks"

    nodes_per_pod = tor_down*int(fabric_radix/2)
    n_nodes = n_pods * nodes_per_pod

    fabrics_per_pod = n_plane
    tors_per_pod = int(fabric_radix/2)

    uplinks_per_fabric = int(fabric_radix/2/osr)
    spines_per_plane = max(int((n_pods*uplinks_per_fabric)/spine_radix), 1)

    n_tors = tors_per_pod * n_pods
    n_fabrics = fabrics_per_pod * n_pods
    n_spines = spines_per_plane * n_plane

    print("nodes: %d"%n_nodes)
    print("tors: %d"%n_tors)
    print("fabrics: %d"%n_fabrics)
    print("spines: %d (%d/plane)"%(n_spines, spines_per_plane))
    print("total switches: %d"%(n_spines + n_fabrics + n_tors))

    # ~~~
    # build the tree
    # ~~~
    xp = xir.Network({})

    # define the nodes and switches

    nodes = [xp.addNode(node('h%d'%i)) for i in range(n_nodes)]
    tors = [xp.addNode(torswitch('tor%s'%i, tor_down, tor_up, node_bw)) for i in range(n_tors)]
    fabrics = [xp.addNode(coreswitch('fabric%s'%i, fabric_radix, core_bw)) for i in range(n_fabrics)]
    spines = [xp.addNode(coreswitch('spine%s'%i, spine_radix, core_bw)) for i in range(n_spines)]

    # connect up topology

    #TODO no longer working with node as subnetwork XXX
    #for i,n in enumerate(nodes):
    #    sp = tors[math.floor(i/tor_down)].endpoints[i%tor_down]
    #    nic = list(
    #            filter(lambda x: x.props['pci_class'] == PciClass.network_controller, n.props['cards'])
    #          )[0]

    #    xp.link([nic.endpoints[0], sp], {})

    for i,t in enumerate(tors):
        for p in range(n_plane):
            j = i*n_plane + p
            fdown = math.floor(fabric_radix/2)
            fp = fabrics[math.floor(j/fdown)].endpoints[j%fdown]
            xp.link([t.endpoints[48+p], fp], {})


    r = math.floor(fabric_radix/2)
    for i,f in enumerate(fabrics):
        for p in range(r):
            j = i*r + p
            sp = spines[math.floor(j/spine_radix)].endpoints[j%spine_radix]
            xp.link([f.endpoints[r+p], sp], {})

    return xp

