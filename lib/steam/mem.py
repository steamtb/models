import xir

def ddr4(size, speed):
    x = xir.Node({ 'model': 'Generic DDR4 DIMM', 'size': size })
    x.endpoint({
        'protocol': 'ddr4',
        'clock': speed
    })
    return x

