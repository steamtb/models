# A parametric fattree model
#
# author: rgoodfel
#
# This model was developed as a part of the ARPA-E Enlitened Program.
# Apache 2.0 License
#

set ns [new Simulator]
source tb_compat.tcl

# Base Parameters
set N_NODE 16
set SWITCH_RADIX 8
set BW 1Gb

# Derived Parameters
set N_LEAF [expr $N_NODE/($SWITCH_RADIX/2)]
set N_SPINE [expr $N_LEAF/2]
set BOND_SIZE [expr $SWITCH_RADIX/$N_SPINE]

for {set i 0} {$i < $N_LEAF} {incr i} {
  set leaf($i) [$ns node]
}

for {set i 0} {$i < $N_SPINE} {incr i} {
  set spine($i) [$ns node]
}

for {set i 0} {$i < $N_SPINE} {incr i} {
  for {set j 0} {$j < $N_LEAF} {incr j} {
    for {set k 0} {$k < $BOND_SIZE} {incr k} {
      set trk-l$j-s$i-$k [$ns duplex-link $leaf($j) $spine($i) $BW 0ms DropTail]
    }
  }
}

for {set i 0} {$i < $N_NODE} {incr i} {
  set n($i) [$ns node]
  set l [expr $i%$N_LEAF]
  set lnk($i) [$ns duplex-link $n($i) $leaf($l) $BW 0ms DropTail]
}

$ns rtproto Static
$ns run
