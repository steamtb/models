import xir
from .pci import PciClass

def nic10G(neth, npci):
    x = xir.Node({
        'model': 'Generic 10G NIC',
        'pci_class': PciClass.network_controller
        })

    for _ in range(neth):
        x.endpoint({'protocol': 'ethernet', 'speed': xir.gbps(10)})

    #for _ in range(npci):
    #    x.endpoint({'protocol': 'pcie3', 'lanes': xir.eq(1)})

    x.endpoint({'protocol': 'pcie3', 'phy': 'pcie_x%d'%npci, 'lanes': npci})

    return x

def switch(name, ports):

    x = xir.Node({
        'name': name,
        'model': 'Generic %dG %d radix switch'%( ports[0].props['speed'], len(ports) )
    }, ports)

    return x

