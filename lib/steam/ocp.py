import xir
from .net import switch

###############################################################################
#
#   Project Olympus
#
###############################################################################

def olympus(name, cpu, memory, cards):

    olympus_validate_memory(memory)

    procs = [cpu(), cpu()]
    procs[0].props['name'] = 'proc0'
    procs[1].props['name'] = 'proc1'

    n = xir.Network({
        'name': name,
        'type': 'computer',
        'vendor': 'OCP',
        'model': 'Olympus',
        })

    n.addNode(procs[0])
    n.addNode(procs[1])
    for bank in memory:
        for dimm in bank:
            n.addNode(dimm)

    for card in cards:
        n.addNode(card['card'])

    # link memory to cpu ddr4 ports
    for b,bank in enumerate(memory):
        ddr_ports = xir.protoFilter(procs[b], 'ddr4')
        for d,dimm in enumerate(bank):
            n.link([ddr_ports[d], dimm.endpoints[0]], {})

    # create pcie slots
    pci_slots = [
            #0
            n.node({'name': 'PCI #1', 'model': 'PCIe x8 Slot'}, [
                xir.Endpoint({'phy': 'pcie_x8_cnx', 'protocol': 'pcie3', 'lanes': 8}),
                xir.Endpoint({'phy': 'pcie_x8', 'protocol': 'pcie3', 'lanes': 8})
            ]),
            #1
            n.node({'name': 'PCI #2', 'model': 'PCIe x8 Slot'}, [
                xir.Endpoint({'phy': 'pcie_x8_cnx', 'protocol': 'pcie3', 'lanes': 8}),
                xir.Endpoint({'phy': 'pcie_x8', 'protocol': 'pcie3', 'lanes': 8})
            ]),
            #2
            n.node({'name': 'PCI #3', 'model': 'PCIe x16 Slot'}, [
                xir.Endpoint({'phy': 'pcie_x16_cnx', 'protocol': 'pcie3', 'lanes': 16}),
                xir.Endpoint({'phy': 'pcie_x16', 'protocol': 'pcie3', 'lanes': 16})
            ]),
            #3
            n.node({'name': 'PCI #4', 'model': 'PCIe x32 Slot'}, [
                xir.Endpoint({'phy': 'pcie_x32_cnx', 'protocol': 'pcie3', 'lanes': 32}),
                xir.Endpoint({'phy': 'pcie_x16', 'protocol': 'pcie3', 'lanes': 16}),
                xir.Endpoint({'phy': 'pcie_x16', 'protocol': 'pcie3', 'lanes': 16})
            ]),
            #4
            n.node({'name': 'PCI #5', 'model': 'PCIe x16 Slot'}, [
                xir.Endpoint({'phy': 'pcie_x16_cnx', 'protocol': 'pcie3', 'lanes': 16}),
                xir.Endpoint({'phy': 'pcie_x16', 'protocol': 'pcie3', 'lanes': 16})
            ]),
            #5
            n.node({'name': 'PCI #6', 'model': 'Oculink x8 Connector'}, [
                xir.Endpoint({'phy': 'oculink', 'protocol': 'pcie3', 'lanes': 8}),
                xir.Endpoint({'phy': 'pcie_x8', 'protocol': 'pcie3', 'lanes': 8})
            ]),
            #6
            n.node({'name': 'M.2 #1', 'model': 'M.2 x4 Connector'}, [
                xir.Endpoint({'phy': 'm2', 'protocol': 'pcie3', 'lanes': 4}),
                xir.Endpoint({'phy': 'pcie_x8', 'protocol': 'pcie3', 'lanes': 4})
            ]),
            #7
            n.node({'name': 'M.2 #2', 'model': 'M.2 x4 Connector'}, [
                xir.Endpoint({'phy': 'm2', 'protocol': 'pcie3', 'lanes': 4}),
                xir.Endpoint({'phy': 'pcie_x8', 'protocol': 'pcie3', 'lanes': 4})
            ]),
            #8
            n.node({'name': 'M.2 #3', 'model': 'M.2 x4 Connector'}, [
                xir.Endpoint({'phy': 'm2', 'protocol': 'pcie3', 'lanes': 4}),
                xir.Endpoint({'phy': 'pcie_x8', 'protocol': 'pcie3', 'lanes': 4})
            ]),
            #9
            n.node({'name': 'M.2 #4', 'model': 'M.2 x4 Connector'}, [
                xir.Endpoint({'phy': 'm2', 'protocol': 'pcie3', 'lanes': 4}),
                xir.Endpoint({'phy': 'pcie_x8', 'protocol': 'pcie3', 'lanes': 4})
            ]),
    ]

    # connect pcie slots to cpu pcie ports
    pci_ports = list(map(lambda x: xir.protoFilter(x, 'pcie3'), procs))

    # cpu0
    n.link([pci_slots[0].endpoints[1], pci_ports[0][0]], {'lanes': [0,7]}) # pci#1 -> cpu 0 ~ port p0[0:7]
    n.link([pci_slots[1].endpoints[1], pci_ports[0][1]], {'lanes': [0,7]}) # pci#2 -> cpu 0 ~ port p1[0:7]
    n.link([pci_slots[2].endpoints[1], pci_ports[0][4]], {'lanes': [0,7]}) # pci#3 -> cpu 0 ~ port p4[0:7]
    n.link([pci_slots[2].endpoints[1], pci_ports[0][5]], {'lanes': [0,7]}) # pci#3 -> cpu 0 ~ port p5[0:7]
    n.link([pci_slots[7].endpoints[1], pci_ports[0][7]], {'lanes': [0,3]}) # m2#2  -> cpu 0 ~ port p7[0:4]
    n.link([pci_slots[6].endpoints[1], pci_ports[0][7]], {'lanes': [4,7]}) # m2#1  -> cpu 0 ~ port p7[4:8]

    # cpu1
    n.link([pci_slots[3].endpoints[1], pci_ports[1][0]], {'lanes': [0,7]}) # pci#4 -> cpu 1 ~ port p0[0:7]
    n.link([pci_slots[3].endpoints[1], pci_ports[1][1]], {'lanes': [0,7]}) # pci#4 -> cpu 1 ~ port p1[0:7]
    n.link([pci_slots[3].endpoints[2], pci_ports[1][4]], {'lanes': [0,7]}) # pci#4 -> cpu 1 ~ port p4[0:7]
    n.link([pci_slots[3].endpoints[2], pci_ports[1][5]], {'lanes': [0,7]}) # pci#4 -> cpu 1 ~ port p5[0:7]
    n.link([pci_slots[4].endpoints[1], pci_ports[1][2]], {'lanes': [0,7]}) # pci#5 -> cpu 1 ~ port p2[0:7]
    n.link([pci_slots[4].endpoints[1], pci_ports[1][3]], {'lanes': [0,7]}) # pci#5 -> cpu 1 ~ port p3[0:7]
    n.link([pci_slots[8].endpoints[1], pci_ports[1][6]], {'lanes': [0,3]}) # m2#3  -> cpu 1 ~ port p6[4:8]
    n.link([pci_slots[9].endpoints[1], pci_ports[1][6]], {'lanes': [4,7]}) # m2#4  -> cpu 1 ~ port p6[0:4]
    n.link([pci_slots[5].endpoints[1], pci_ports[1][7]], {'lanes': [0,7]}) # pci#5 -> cpu 1 ~ port p7[0:7]

    # multi-socket communication
    for i in range(8):
        n.link([pci_ports[0][i], pci_ports[1][i]], {'lanes': [0,7]})

    # link cards to pcie ports
    for c,card in enumerate(cards):
       slot = card['slot']
       n.link([pci_slots[slot].endpoints[0], xir.protoFilter(card['card'], 'pcie3')[0]], {})

    return n


def olympus_validate_memory(dimms):

    if len(dimms) != 2:
        raise('olympus memory must be organized into 2 banks - 1 for each CPU')

    for x in dimms:
        if len(x) != 16:
            raise(
                'olympus memory banks must specify all 16 slots per proc'+
                'slots may be left empty by placing None at the slot index'
            )

###############################################################################
#
#   Edgecore AS5712
#
###############################################################################

def edgecore10G(name):
    ports = [xir.Endpoint({'type': 'ethernet', 'speed': xir.gbps(10)}) for _ in range(48)]
    ports += [xir.Endpoint({'type': 'ethernet', 'speed': xir.gbps(40)}) for _ in range (4)]
    return switch(name, ports)
