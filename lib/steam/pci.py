import xir
from enum import IntEnum

class PciClass(IntEnum):
    legacy = 0x0
    mass_storage_controller = 0x1
    network_controller = 0x2
    display_controller = 0x3
    multimedia_device = 0x4
    memory_controller = 0x5
    bridge_device = 0x6
    simple_communication_controller = 0x7
    base_system_device = 0x8
    input_device = 0x9
    docking_station = 0xA
    processor = 0xB
    serial_bus_controller = 0xC
    wireless_controller = 0xD
    intelligent_io_controller = 0xE
    satellite_communications_controller = 0xF
    encryption_controller = 0x10
    data_acquisition_signal_processor = 0x11
    black_sheep = 0xFF



