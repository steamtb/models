import xir

def epyc7601():
    x = xir.Node({
        'model': 'AMD Epyc 7601',
        'cores': 16,
        'threads_per_core': 2,
        'clock': xir.between(2.2, 3.2),
        'l3': xir.mB(64),
        'tdp': xir.w(180)
        })

    for i in range(4): # 4 dies per cpu
        for j in range(4): # 4 x8 PCIe PHYs per die
            x.endpoint({
                'name': 'pci[%d:%d]'%(i,j),
                'protocol': 'pcie3',
                'phy': 'pcie_x8',
                'lanes': 8
            })

    for i in range(4): # 4 dies per cpu
        for j in range(2): # 2 DDR4 PHYs per die
            for k in range(2): # 2 DIMMS per channel
                x.endpoint({
                    'name': 'ddr[%d:%d:%d]'%(i,j,k),
                    'protocol': 'ddr4',
                    'clock': xir.between(xir.mt(1333), xir.mt(3200))
                })

    return x


#def cpuPorts(x, proto):
#    return list(filter(lambda e: e.props['protocol'] == proto, x.endpoints))
