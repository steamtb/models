# Steam Models

This repo collects a number of integrated models for use in the Steam project.

## Olympus

Below is a network diagram of [OCP Olympus](http://www.opencompute.org/wiki/Server/ProjectOlympus) node [model](olympus_test.py).

![](doc/olympus.png)
