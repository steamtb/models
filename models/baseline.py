import steam, xir

# Dual Socket OCP Olympus System with 2TB of memory
def compute_node(name):
    return steam.olympus(
            name = name, 
            cpu = steam.epyc7601,
            memory = [
                [steam.ddr4(xir.gB(64), xir.mt(3200)) for _ in range(16)],
                [steam.ddr4(xir.gB(64), xir.mt(3200)) for _ in range(16)] ],
            cards = [ {'card': steam.nic10G(neth=1, npci=4), 'slot': 1} ]
    )

#print(json.dumps(x.xir_dict(), indent=2))

topo = steam.fbfattree_build(
        n_pods = 2,
        tor_down = 48,
        tor_up = 8,
        fabric_radix = 32,
        spine_radix = 32,
        n_plane = 4,
        osr = 1,
        node_bw = 10,
        core_bw = 100,
        node = compute_node
    );

with open("baseline.json", "w") as f:
    f.write(topo.xir())

#print(topo.xir())


