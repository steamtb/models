import steam, xir, json

def mem(name):
    m = steam.ddr4(xir.gB(64), xir.mt(3200))
    m.props['name'] = name
    return m

def nic(name):
    n = steam.nic10G(neth=1, npci=4)
    n.props['name'] = name
    return n

x = steam.olympus(
        name = 'sven', 
        cpu = steam.epyc7601,
        memory = [
            [mem('dimm[0:%d]'%(i)) for i in range(16)],
            [mem('dimm[1:%d]'%(i)) for i in range(16)] ],
        cards = [ {'card': nic('nic10G_0'), 'slot': 0} ] 
    )

with open("sven.json", "w") as f:
    f.write(json.dumps(x.xir_dict(), indent=2))
